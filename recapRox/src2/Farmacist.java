public class Farmacist {

    String nume;
    int grad;
    boolean rezi;
    int vechime;
    int salariu;


    public Farmacist(String nume, int grad, boolean rezi, int vechime, int salariu) {
        this.nume = nume;
        this.grad = grad;
        this.rezi = rezi;
        this.vechime = vechime;
        this.salariu = salariu;
    }

        public String diriginte(){
            if (vechime >= 5) {
               return nume + " Acest farmacist poate fi diriginte ";
            } else {
                return nume + " Acest farmacist NU poate fi diridinte ";
            }
        }

      public String meritaMarire() {
         if (salariu <= 3500){
             return nume + " merita sa avanseze in ierarhia sclaviei";
         }else {
             return nume + " sclavul este pe treapta sociala cuvenita";
         }

    }

       public Farmacist(){
        nume= "madalina";
        grad= 1;
        rezi = true;
        vechime = 2;
        salariu = 3500;}

}
