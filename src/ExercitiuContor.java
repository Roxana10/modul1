import java.util.Scanner;

public class ExercitiuContor {
    public static void main(String[] args) {


        Scanner keyboard =  new Scanner(System.in);

        int contor = 0;
        int suma= 0;
        int produs= 1;
        int no = keyboard.nextInt();
        while (no!=0) {
            suma = suma + no;
            contor = contor +1;
            produs = produs*no;
            no = keyboard.nextInt();

        }

        double mediaAritmetica = ((double) suma)/contor;
        System.out.println("Am citit in total " + contor
         + " numere care au suma " + suma
         + " si media aritmetica " + mediaAritmetica
         + " si produsul este " + produs );

    }
}
