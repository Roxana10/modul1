public class InstructiuneaIf {
    public static void main(String[] args)
    {
       int a, b;
       a=2;
       b=4;

       if (a==b)
       {
           System.out.println("a este egal cu b");
       } else{
           System.out.println("a nu este egal cu b");
       }

       if (a!=b)
       {
           System.out.println("a este diferit de b");
       }else{
           System.out.println("a nu este diferit de b");
       }


       int variabila = 8;
       if (variabila>=9){
           int dublul = 2 *variabila;
           System.out.println("variabila este mai mare decat 9 iar dublul sau este " + dublul);
       }else{
           System.out.println("variabila nu este mai mare decat 9 si nu se poate calcula dublul ei");
       }

        int jumatate = -1;
        if (variabila <= 11) {
            jumatate = variabila / 2;
            System.out.println("variabila e mai mică decât 11 iar jumătatea ei este " + jumatate);
        }

        // Exercițiul 1
        // calculați dacă un elev are medie de trecere


        float nots1, nots2, nots3, nots4;
        float medi;

           //cazul 1
        nots1=2.345689f;
        nots2=3.12345677f;
        nots3=5.123456789f;
        nots4=8.12398765f;
        medi = (nots1+nots2+nots3+nots4)/4;

        System.out.println("media= " + medi);

        // afișați mesajul "Materie promovată"  în cazul în care media e >= 4.5 sau "Necesită mărire de notă" în caz contrar
        if (medi>=5) {
            System.out.println("materie promovata cu media " + medi);
        }else{
            System.out.println("necesita marire de nota ");}

        // adițional dacă media este 10 afișați "Necesită bursă"
        if (medi==10){
            System.out.println("necesita bursa");
        }

           //cazul 2 -  folosim "if else"

        nots1=9;
        nots2=6;
        nots3=5;
        nots4=9;
        medi = (nots1+nots2+nots3+nots4)/4;
        // afișați mesajul "Materie promovată"  în cazul în care media e >= 4.5 sau "Necesită mărire de notă" în caz contrar
        if (medi>=5) {
            System.out.println("materie promovata cu media " + medi);
        }else if (medi==10){
            System.out.println("necesita marire de nota ");}
          else{System.out.println("necesita bursa");}
        // adițional dacă media este 10 afișați "Necesită bursă"


        // Exercițiul 2
        // se dau două numere pe care le considerăm laturile unei camere.
        // daca CEL PUȚIN una dintre laturile camerei e mai mare de 3m ȘI
        // suprafata camerei este mai mare de 9m2 atunci afișăm "E o cameră", altfel afișma "E un birou"

          int lungime, latime, suprafataPodeaTavan;

          lungime= 2;
          latime = 2;
          suprafataPodeaTavan = lungime*latime;
           if ((lungime>3 || latime>3) && suprafataPodeaTavan>9){
               System.out.println("este o camera");}
           else {
               System.out.println("este un birou");
           }

           boolean lungime1, latime1, suprafata1;
           lungime1= lungime>3;
           latime1= latime>3;
           suprafata1 = suprafataPodeaTavan>9;
          if ((lungime1 || latime1) && suprafata1)
          {System.out.println("aceasta este o camera");}
          else
          {System.out.println("acesta este un birou");}

          //exercitiu 2 cazul b
          // calculati daca avem destula vopsea sa vopsim peretii

          double vopseaDisponibila;
          double inaltime;

          vopseaDisponibila= 15;
          inaltime =2.5;

          boolean vopseaNecesara;
          double  perete1, perete2;
          perete1 = inaltime*latime;
          perete2 = (inaltime*lungime);
          vopseaNecesara = perete1*perete2<=15;

        // Exemplu if else if
        // if ....... if else .......  if else if
        // < 6 m2 nu poate fi decat baie
        // 6 - 9 m2 birou
        // >9 m2 e o camera

        int l1, l2, r, s1, s2;
          //l1= latura 1,  l2=latura 2, r= aria tavan/podea, s1=suprafata perete 1,  s2=suprafata perete 2, h=inaltimea
        int h;
        l1 =2;
        l2=3;
        h=2;
        r = l1*l2;
        s1 = h*l1;
        s2 =h*l2;

        if (r<6) {
            System.out.println("nu poate fi decat baie");}
        else if (r>5 && r<9){
            System.out.println("camera");
            System.out.println("birou");}


        // Exercițiul 3
        // un barbat vrea sa mearga in club.
        // ca sa intre in club trebuie sa fie major (>=18).
        // Ca sa poata comanda alcool trebuie sa aibe cel putin 21 de ani.
        // ??? poate intra in club si poate sa bea,  poate intra in club dar nu poate bea, nu poate intra in club
        // are X bani, si o sampanie costă Y bani.
        // ?? poate intra in club si poate sa bea sampanie
        // poate intra in club si poate sa bea dar nu sampanie


        int  age, bani,sampanie;
        age= 29;
        bani = 4;
        sampanie = 32;

        if (age>=18 ){System.out.println(" poate intra in club");}
        else{
            System.out.println(" nu poate intra in club");
         }

            if (age<21 && age>18) {
                System.out.println("Poate intra in club, dar nu poate consuma alcool");}
                else if (age<18){
                    System.out.println(" Nu poate intra in club si nu poate consuma alcool");}
                else {

                    System.out.println(" Poate consuma Alcool");}

                    boolean ageok, baniok, sampanieok;
                ageok= age>21;
                baniok= sampanie<bani;

                if (ageok && baniok)
                {System.out.println("Poate comanda sampanie pentru ca are bani");}
                else if (!ageok && baniok)
                { System.out.println("Are bani dar nu si varsata corespunzatoare ");}
                 else if (ageok && !baniok)
                  {System.out.println("Are varsta necesara, dar nu are bani");}






    }

    }


