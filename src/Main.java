public class Main {
    public static void main(String[] args)
    {
        //am declarat variabila de date "x", care are tipul de date "int"
        int x;
        //am asignat valoarea 5 variabilei x
            x=5;
            int a_1=13, b_2=14;
            x=a_1 + b_2;
            x=x+1;
            //afisam textul dintre ghilimele
        System.out.print("x=");
        //afisam valoarea variabilei x

        long max = 5000000000l;
                max=max+max;
                System.out.print("max =");
                System.out.println(max);
        System.out.println(x);
        double a=14.5, b=2.56;
        //printeaza mot-a-mot ce scrie intre paranteze
        System.out.println("a+b");
        //efectuează calculul si afiseaza rezultatul
        System.out.println(a+b);
        float ff = 4.342175438126347809734356738386f;
        System.out.println(ff+x);

        boolean m = true, n = false;
        boolean result;
        //m AND n adevarat doar cand ambele sunt adevarate
        result=m&&n;
        System.out.print("m&&n=");
        System.out.println(result);
        //m or N adevarat cand cel putin iuna e adevarata

        result= m||n;
        System.out.print("m||n=");
        System.out.print(result);

        x=5; x=44;
        n=true; n=false;

        boolean v;
        //boolean v; <-incorect! nu pot avea doua variabile cu acelasi nume
        //boolean v=44, v=66; <-incorect! nu pot avea doua variabile cu acelasi nume

        char c1='g';
        System.out.println(c1);

        String cuvant= "pere";
        String propozitie = "Salutare, vrei pere?";
        //propozitie=false; <-incorect, signarea, nu functioneaza

        System.out.println(propozitie);
        propozitie="alta valoare pt propozitie";
        System.out.println(propozitie);

        propozitie="salut!" + "Eu sunt Marcel!";
        System.out.println(propozitie);
        propozitie=propozitie+"pe tine cum te cheama";
        System.out.println(propozitie);

        // exemplu
        int p, j;
        p=14;
        j=12;
        int suma= p+j;
        System.out.print("p+j=");
        System.out.println(suma);
        System.out.println("p+j="+suma); // p+j=26   este un string + un int. se considera plusul ca concatenare (alipire)
        System.out.print(p+j); //considerul +, semnulplus d ela adunare
        System.out.println("p+j="+p+j); //string+int+int
        System.out.println("p+j="+(p+j));


        // exercitiu:  scrie de unul singur cate un exemplu d evariabile
                int z, z_1=10, z_2=15;
                z=2;
                z=z_2-z_1;
                z=z+z_1;
                int y;
                y=4;
                System.out.print("z=");
                System.out.println(z);

                double  r, c;
                r=12.34;
                c=56.78;
                System.out.print("a=");
                System.out.println(c-r);


                float s;
                s=z_2/z_1;
                System.out.println("12 impartit la 10 este" + s);

                float dd = 4.45344235456784345f;
                float noulDd;
                noulDd= ff+2;
                System.out.print("noul Dd este egal cu");
                System.out.println(dd+2);

                long maxim=1000000000l;
                maxim=maxim+maxim;
                System.out.print("maxim=");
                System.out.println(maxim);

                int numar;
                numar = 1252288669;
                System.out.print("numar=");
                System.out.println(numar+numar-1);

                short numarScurt=34;
                System.out.print("numarScurt=");
                System.out.println(numarScurt*2);


                char ch = '8';
                System.out.println(ch);


                String cuvantul = "pere";
                String propzitie = "Salutare, vrei pere?";
                // cuvant = false; <- incorect, asignarea nu funcționează
                System.out.println(propzitie);
                propzitie = "Altă valoare pentru propozitie.";
                System.out.println(propzitie);

                propzitie = "Salut! " + "Eu sunt Marcel! ";
                System.out.println(propzitie);
                propzitie = propzitie + " Pe tine cum te cheamă?";
                System.out.println(propzitie);
                System.out.println("Pe mine " + "mă cheamă " + "Anton.");
    }}