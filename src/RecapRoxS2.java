import java.util.Scanner;

public class RecapRoxS2 {

    public static void main(String[] args) {
        // vrem sa afisam 100 de  ^
        //vrem sa afisam 1 2 3 4 5 6.... pana la 100

        //loop instruction

        int i;

        for (i = 1; i <= 100; i++) {
            System.out.print("^");
        }
        System.out.println(" ");


        // scriu nr impare de la 1 la 20 cu for apoi cu while
        System.out.print(" For: ");
        System.out.println(" ");
        for (i = 1; i <= 20; i++)
            if (i % 2 == 1) {
                System.out.print(i + " ");
            }
        System.out.println(" ");

        System.out.print(" While: ");
        System.out.println(" ");
        int nr;
        nr = 1;
        while (nr <= 19) {
            System.out.print(nr + " ");
            nr += 2;
        }

        System.out.println(" ");


        //vrem sa afisam 1 2 3 4 5 6.... pana la 100 cu for apoi cu while
        System.out.print(" For: ");
        System.out.println(" ");
        for (i = 1; i <= 100; i++) {
            System.out.print(i + " ");
        }

        System.out.println(" ");


        System.out.print(" While: ");
        System.out.println(" ");

        int nrr = 1;
        while (nrr <= 100) {
            System.out.print(nrr + " ");
            nrr++;
        }
        System.out.println(" ");


        //vrem sa afisam 100 99,98, 97...2,1 cu for apoi cu while
        System.out.print(" For: ");
        System.out.println(" ");
        for (i = 100; i >= 1; i--) {
            System.out.print(i + " ");
        }
        System.out.println(" ");


        System.out.print(" While: ");
        System.out.println(" ");
        int e = 100;
        while (e >= 1) {
            System.out.print(e + " ");
            e--;
        }
        System.out.println(" ");


        //vrem sa afisam crescator numerele pare pana la 100 cu for apoi cu while

        System.out.print(" For: ");
        System.out.println(" ");
        for (i = 0; i <= 100; i++)
            if (i % 2 == 0) {
                System.out.print((i) + " ");
            }
        System.out.println(" ");

        System.out.print(" While: ");
        System.out.println(" ");
        int r = 0;
        while (r <= 100) {
            System.out.print(r + " ");
            r = r + 2;
        }
        System.out.println(" ");


        // vrem sa afisam descrescator numerele pare de la 100 cu for apoi cu while.
        System.out.print(" For1: ");
        System.out.println(" ");
        for (i = 100; i >= 0; i--)
            if (i % 2 == 0) {
                System.out.print((i) + " ");
            }
        System.out.println(" ");

        System.out.print(" While: ");
        System.out.println(" ");


        int f = 100;
        while (f >= 1) {
            System.out.print(f + " ");
            f = f - 2;
        }
        System.out.println(" ");

        // vrem sa afisam crescator numerele impare de la 1 la 100 cu for apoi cu while
        System.out.print(" For: ");
        System.out.println(" ");
        for (i = 0; i <= 100; i++)
            if (i % 2 == 1) {
                System.out.print((i) + " ");
            }
        System.out.println(" ");


        System.out.print(" While: ");
        System.out.println(" ");
        int t = 1;
        while (t <= 100) {
            System.out.print(t + " ");
            t = t + 2;
        }
        System.out.println(" ");


        // vrem sa afisam descrescator numerele impare de la 100
        System.out.print(" For: ");
        System.out.println(" ");
        for (i = 100; i >= 0; i--)
            if (i % 2 == 1) {
                System.out.print(i + " ");
            }
        System.out.println(" ");

        System.out.print(" While: ");
        System.out.println(" ");
        int j = 99;
        while (j >= 1) {
            System.out.print(j + " ");
            j = j - 2;
        }
        System.out.println(" ");


        //vrem sa afisam crescator de la 1 la 100 nr din 3 in 3;
        System.out.print(" For: ");
        System.out.println(" ");
        for (i = 1; i <= 100; i = i + 2)
            System.out.print(i + " ");
        System.out.println(" ");

        System.out.print(" While: ");
        System.out.println(" ");
        int h = 1;

        while (h <= 100) {
            System.out.print(h + " ");
            h = h + 2;
        }
        System.out.println(" ");


        //vrem sa afisam descrescator nr divizibile cu 5 de la 100 la 1;
        System.out.print(" For: ");
        System.out.println(" ");
        for (i = 100; i >= 0; i--) {
            if (i % 5 == 0) {
                System.out.print(i + " ");
            }
        }
        System.out.print(" ");
        System.out.println(" ");

        System.out.print(" While: ");
        System.out.println(" ");
        int k = 100;
        while (k >= 1) {
            System.out.print(k + " ");
            k = k - 5;
        }

        System.out.println(" ");


        // cereti de la tastatura 3 numere in felul următor
        // capt inferior al intervalului
        // capat superior al intervalului
        // numar cu care sa se dividă
        // 13 967 25 <- toate numerele intre 13 si 967 care se divid cu 25

        Scanner ceva = new Scanner(System.in);
        System.out.println(" Scrieti primul numar ");
        int y = ceva.nextInt();


        System.out.print(" ");
        System.out.println(" ");


        Scanner tastatura = new Scanner(System.in);
        int capatInferior, capatSuperior, divizor;
        System.out.print(" Tastati capatul inferior al intervalului ");
        capatInferior = tastatura.nextInt();
        System.out.print(" Tastati capatul superior al intervalului ");
        capatSuperior = tastatura.nextInt();
        System.out.print(" Tastati divizorul ");
        divizor = tastatura.nextInt();

        for (i = capatInferior; i <= capatSuperior; i++)
            if (i % divizor == 0) {
                System.out.print(i + " ");
            }

        System.out.print(" ");
        System.out.println(" ");

        Scanner altaTastatura = new Scanner(System.in);
        int a, b, c;

        System.out.print(" a de pe alta tastatura ");
        a = altaTastatura.nextInt();
        System.out.print(" b de pe alta tastatura ");
        b = altaTastatura.nextInt();
        System.out.print(" c de pe alta tastatura ");
        c = altaTastatura.nextInt();

        // suma de la 2 la 7
        int suma = 0;
        for (i = 2; i <= 7; i++) {
            System.out.println(" S1 " + (suma = suma + i) + " ");
        }

        System.out.println(" ");

        // suma nr de la 2 la 5
        int sum = 0;
        for (i = 2; i <= 5; i++) {

            System.out.print(" S2 " + (sum = sum + i) + " ");
        }


// Selectați 1 pentru română, 2 pentru engleză. For english press 2, for romanian press 1.
// 1
// Bun venit!

// 2
// Welcome

// *orice alt număr
// Selectați 1 pentru română, 2 pentru engleză. For english press 2, for romanian press 1.


        Scanner tast = new Scanner(System.in);
        int apasare=0;

        while(apasare!=1 && apasare!=2) {
            System.out.println(" Pentru Romana, apasati atsta 1. Pentru Engleza apasati tasta 2. " +
                    "For English press 2, for Romanian press 1. ");
            apasare = tast.nextInt();

        }
        if(apasare == 1) {
            System.out.println("Bun venit!");}
        if (apasare == 2) {
            System.out.println("Welcome!");
        }




    }}



