public class Tema1 {
    public static void main(String[] args)
    {
        //Declarați 3 numare ca fiind vârstele unor oameni.
        //1. Afișați dacă toți oamenii pot merge într-un club. (fiecare dintre ei e major)
        //2. Afișați dacă oamenii pot să se îmbete. (cel puțin unul dintre ei are 21 de ani)

        int  age1, age2, age3;
        age1= 9;
        age2 = 14;
        age3 = 2;

        boolean ageok1, ageok2, ageok3;
        ageok1= age1>=18;
        ageok2= age2>=18;
        ageok3= age3>=18;


        if (age1>=18 && age2>=18 && age3>=18)
               {System.out.println("Toata lumea poate intra in club");}
        else if (!ageok1 && ageok2 && ageok3)
               System.out.println("Persoana nr 1 nu poate intra in club, dar celelate 2 da");
        else if (ageok1 && !ageok2 && ageok3)
            System.out.println("Persoana nr 2 nu poate intra in club, dar celelate 2 da");
        else if (ageok1 && ageok2 && !ageok3)
            System.out.println("Persoana nr 3 nu poate intra in club, dar celelate 2 da");
        else if (!ageok1 && !ageok2 && ageok3)
            System.out.println("Persoana nr 3  poate intra in club, dar celelate 2 nu");
        else if (!ageok1 && ageok2 && !ageok3)
            System.out.println("Persoana nr 2  poate intra in club, dar celelate 2 nu");
        else if (ageok1 && !ageok2 && !ageok3)
            System.out.println("Persoana nr 1  poate intra in club, dar celelate 2 nu");
        else if (!ageok1 && !ageok2 && !ageok3)
            System.out.println("Niciuna dintre cele 3 persoane nu poate intra in club");

        boolean ageok11, ageok22, ageok33;
        ageok11= age1>=21;
        ageok22= age2>=21;
        ageok33= age3>=21;

        if (age1>=21 && age2>=21 && age3>=21)
        {System.out.println("Toata lumea se poate imbata in club");}
        else if (!ageok11 && ageok22 && ageok33)
            System.out.println("Persoana nr 1 nu se poate imbata in club, dar celelate 2 da. Persoana nr 1 boate bea doar acasa");
        else if (ageok11 && !ageok22 && ageok33)
            System.out.println("Persoana nr 2 nu se poate imbata in club, dar celelate 2 da. Persoana nr 2 boate bea doar acasa");
        else if (ageok11 && ageok22 && !ageok33)
            System.out.println("Persoana nr 3 nu se poate imbata in club, dar celelate 2 da. Persoana nr 3 boate bea doar acasa");
        else if (!ageok11 && !ageok22 && ageok33)
            System.out.println("Persoana nr 3  poate sa se imbete in club, dar celelate 2 nu");
        else if (!ageok11 && ageok22 && !ageok33)
            System.out.println("Persoana nr 2  poate sa se imbete in club, dar celelate 2 nu");
        else if (ageok11 && !ageok22 && !ageok33)
            System.out.println("Persoana nr 1  poate sa se imbete in club, dar celelate 2 nu");
        else if (!ageok11 && !ageok22 && !ageok33)
            System.out.println("Niciuna dintre cele 3 persoane nu poate sa " +
                    "se imbete in club. Toata lumea bea acasa la Dan. El are rachiu de pere");


        //Declarați 3 numere ca fiind posibile valori pentru laturile unui triunghi. Declarati si o variabila care reprezintă câtă sârmă avem :D
        //1. Verificați dacă se poate forma un triunghi cu numerele date. (suma oricăror două laturi trebuie să fie mai mare decât a treia latură)
        //2. Dacă se poate forma un triunghi, verificați dacă e triunghi echilateral. (toate laturile egale)
        //3. Daca e triunghi echilateral, verificați dacă avem suficientă sârmă să îl împrejmuim.
        //4. Dacă putem să îl împrejmuim de mai multe ori, afișați de câte ori putem să îl împrejmuim

        int l1, l2, l3, sarmaDisponibila, p;

        // l1,l2 si l3 =laturi, p=perimetru

        l1=1;
        l2=1;
        l3=1;
        sarmaDisponibila=40;
        p=l1+l2+l3;

        boolean s1, s2, s3;

          //s1, s2  si s3 sunt sumele a cate 2 laturi ale triunghiului


        s1 = l1+l2>l3;
        s2 = l1+l3>l2;
        s3 = l2+l3>l1;

               //1.
        if (s1 || s2 || s3)
             {System.out.println("Acesta este un triunghi");}


               //2.
        if (l1==l2 && l1==l3)
            {System.out.println("Acesta este un triunghi echilateral");}
        else
            {System.out.println("Acesta nu este un triunghi echilateral");}



              //3.
        if (l1==l2 && l1==l3)
        {System.out.println("Acesta este un triunghi echilateral");}
        else if (p<=sarmaDisponibila)
        {System.out.println("Acesta nu este un triunghi echilateral");}
        {System.out.println("Acesta este un triunghi echilateral si avem nevoie de " + p + " metri de sarma");}


               //4.
        float l11, l22, l33, sarmaDisponibila1, p1;

        // l1,l2 si l3 =laturi, p=perimetru

        l11=2f;
        l22=4f;
        l33=6f;
        sarmaDisponibila1=40f;
        p1=l11+l22+l33;


        if (p1<=sarmaDisponibila1 && (sarmaDisponibila1/p1!=0))
        {System.out.println("Putem imprejmui triunghiul cu sarma de " + (sarmaDisponibila1/p1) + " ori");}



        //4. Să se scrie un program care determină maximul a două numere întregi.
        int a, b;

        a=4;
        b=24;

        if (a<b)
        {System.out.println(" Maximul dintre a si b este b si are valoarea " + b);}
        else
        {System.out.println( "Maximul dintre a si b este a si are valoarea " + a);}





        //4.1 Fiind date vârstele a doi copii afișați care dintre ei este cel mai mare și cu cât.

        int varsta1, varsta2;

        varsta1=33;
        varsta2=3;

        if (varsta1<varsta2)
        {System.out.println(" Copilul nr2 este mai mare ");}
        else
        {System.out.println( "Copilul nr1 este mai mare ");}

          if (varsta2<varsta1)
          {System.out.println(" difernta dintre copii este de " + (varsta1-varsta2) + " ani");}
          else
          {System.out.println(" difernta dintre copii este de " + (varsta2-varsta1) + " ani");}




        // 5.Determinați câte sticle de x litri cu apă trebuie deschise pentru a umple un vas de y litri.

        float sticle, vas;

        sticle= 2;
        vas= 57;

        System.out.println ("trebuie deschise " + vas/sticle + " sticle");






        //6. Fiind dată nota unui elev să se afișeze dacă acesta este corigent sau promovat.
        double nota=4.51;

        if (nota>4.5 && nota<5.00)
        {System.out.println( "elev promovat");}

        if (nota>=5)
        {System.out.println( "elev promovat ");}
        else
        {System.out.println( "elev corigent ");}




        //7.La un concurs pot participa copii cu vârste între a și b inclusiv. Gigel are n ani. Stabiliți dacă poate participa la concurs.

        int aa, bb, n;

        aa=10;
        bb=17;
        n=6;

        if (n>=aa && n<=bb)
        {System.out.println( "Gigel poate participa la concurs.");}
        else {System.out.println ( " Gigel NU poate participa la concurs. ");}



        //8. Se dau 2 numere reale. Calculați suma, diferenţa, produsul şi câtul lor, în această ordine.

        float nr1, nr2;

        nr1= 4.5f;
        nr2= -7;


        System.out.println( nr1+nr2 );
        System.out.println( nr1-nr2 );
        System.out.println( nr1*nr2 );
        System.out.println( nr1/nr2 );







        //9. Simulați un semafor. Rețineți culoarea semaforului și în funcție de aceasta afișați:
        // "Trecerea oprită" pentru roșu, "Trecerea permisă" pentru verde și "Atenție!" pentru galben.
        // Dacă apare orice altă culoare, afișați "Defecțiune :)"

          String culoare = "rosudgj";

          switch (culoare) {
            case "rosu":
        {System.out.println( "Trecere oprita!");}
            break;
            case "verde":
        {System.out.println( "Trecere permisa!");}
            break;
            case "galben":
        {System.out.println( "Atentie!");}
            break;
            default:
        {System.out.println( "Defectiune!");}     }

       
        
        
        //10. Se dau două numere naturale și un simbol pentru una dintre operațiile +, -, *, /. 
        // Să se determine rezultatul operației aplicate pentru cele două numere.

        
        float number1, number2;
          number1=4.568f;
          number2=4.5f;

        String rezultat = " re";

        switch (rezultat) {
            case " rezultat adunare":
            {System.out.println(" Rezultatul adunarii este " + (number1+number2));}
            break;
            case " rezultat scadere":
            {System.out.println(" Rezultatul scaderii este " + (number1-number2));}
            break;
            case " rezultat inmultire":
            {System.out.println(" Rezultatul inmultirii este " + (number1*number2));}
            break;
            case " rezultat impartire":
            {System.out.println(" Rezultatul impartirii este " + (number1/number2));}
            break;
            default:
            {System.out.println("Eroare. Eu nu stiu matematica");}
            break;
                
            
        }





    }}


