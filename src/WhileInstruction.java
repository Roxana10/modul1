import java.util.Scanner;

public class WhileInstruction {

    public static void main(String[] args) {

        int nrr1 = 4;

        // while condition is true, execute
        while (nrr1 < 10) {
            System.out.print(nrr1 + " ");
            nrr1 = nrr1 + 3;
        }
        // 4 7


        // afisati nr pare de la 0 la 20 (ordine crescatoare)
        int numar = 8;

        while (numar < 10) {
            if (numar % 2 == 0) {
                System.out.print(numar + " ");
            }
            numar = numar + 2;
        }
        System.out.print(" ");


        //afisati nr impare de la 40 la 20 (ordine descrescatoare)
        int nr = 39;
        while (nr > 20) {
            if (nr % 2 == 1) {
                System.out.print(nr + " ");
            }
            nr = nr - 1;

            System.out.print(" ");
        }


        //selectati 1 pt romana. 2 pt engleza. For english press1, for english press 2
        //1 - bun venit

        //2 - welcome

        //*orice alt numar
        //selectati 1 pt romana, 2 pentru engleza. For english press1, for english press 2


        Scanner keyboard = new Scanner(System.in);

        int n;
        System.out.println("selectati 1 pt romana, 2 pentru engleza. For english press1, for english press 2 ");
        n = keyboard.nextInt();

        while (n != 1 && n != 2) {
            System.out.println("selectati 1 pt romana, 2 pentru engleza. For english press1, for english press 2 ");
            n = keyboard.nextInt();
        }

        if (n == 1) {
            System.out.println("Bun Venit");
        }
        if (n == 2) {
            System.out.println("Welcome");
        }


        // cereti de la tastatura 3 numere in felul următor
        // capt inferior al intervalului
        // capat superior al intervalului
        // numar cu care sa se dividă
        // 13 967 25 <- toate numerele intre 13 si 967 care se divid cu 25

        Scanner tastatura = new Scanner(System.in);
        tastatura.nextInt();

        int capatinferior, capatSuperior, divizor;
        System.out.println("tastati primul numar capat de interval ");
        capatinferior = tastatura.nextInt();

        System.out.println("tastati al 2-lea numar capat de interval ");
        capatSuperior = tastatura.nextInt();

        System.out.println("tastati un numar din interval cu care sa se divida variabila ii ");
        divizor = tastatura.nextInt();

       int ii =0 ;
       for (ii = capatinferior; ii<= capatSuperior; ii++) {
           if (ii % divizor == 0) {
        System.out.println(ii + " ");
          }


       }




    }}


