package oop;
import java.time.LocalDate;
import java.util.Locale;

public class PrimitivesAndObjects {


    public static void main(String[] args) {

        int intreg;
        double nrZecimal;
        boolean logica;
        char caracter;

        String sirDeCaractere;
        // string e obict


        sirDeCaractere = "Un sir de caractere ...";
        System.out.println(sirDeCaractere);
        System.out.println(sirDeCaractere.toUpperCase()); //string is mutable
        System.out.println(sirDeCaractere.charAt(3));
        System.out.println(sirDeCaractere.toLowerCase());
        System.out.println(sirDeCaractere.contains( "..."));

        // string pool

        sirDeCaractere = sirDeCaractere + " care are continuare";
        System.out.println(sirDeCaractere);


        //Integer intregObj = new Integer(45); --- asa  s ar scrie d efapt, dar java e destept
        // si ne lasa sa l scriem ca pe linia de mai jos
        Integer intObj=45;
        System.out.println(intObj.byteValue());
        intObj= null;


        Double doubleObj = 12.4;

    }



}
