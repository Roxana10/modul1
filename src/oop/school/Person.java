package oop.school;

import java.time.LocalDate;
import java.time.Period;

public class Person {

    //cnp
    //nume
    //prenume
    //salariu
    //salariul minim pe economie

    // CNP: gen/an/luna/zi      GYYMMDD
    //G=1 sau 2 - persoana e nascuta in 1900+
    //G=5 sau 6v-vpersoana e nascuta in 2000 +

    //cnp ul pentru M incepe cu 1 sau 5
    //cnp ul pentru F incepe cu 2 sau 6


    public static final char[] CNP_FEMEI = {'2', '6'};
    public static final char[] CNP_BARBATI = {'1', '5'};

    public static final int CNP_POZITIE_GEN = 0;


    private String cnp;
    private String nume;
    private String prenume;
    private double salariul;
    private static double salariulMinim;
    private final LocalDate dataNasterii;

    // un constructor public
    //gettere si settere pentru nume, prenume, salariul.
    public boolean isFemale() {

        char gender = cnp.charAt(0);
        if (gender == '2' || gender == '6') {
            return true;
        } else {
            return false;
        }
    }


    public boolean isMale() {
        char gender = cnp.charAt(0);
        if (gender == '1' || gender == '5') {
            return true;
        } else {
            return false;
        }

    }


    // versinea 2 d ela is female

    public boolean isFemale2() {
        char gender = cnp.charAt(CNP_POZITIE_GEN);
        if (gender == CNP_FEMEI[0] || gender == CNP_FEMEI[1]) {
            return true;
        } else {
            return false;
        }
    }

    public boolean isMale2() {
        char gender = cnp.charAt(CNP_POZITIE_GEN);
        if (gender == CNP_BARBATI[0] || gender == CNP_BARBATI[1]) {
            return true;
        } else {
            return false;
        }
    }


    // metoda is Female - varianta 3
    public boolean isFemale3() {
        char gender = cnp.charAt(CNP_POZITIE_GEN);
        for (int i = 0; i < CNP_FEMEI.length; i++) {
            if (CNP_FEMEI[i] == gender) {
                return true;
            }
        }
        return false;
    }

    public boolean isMale3() {
        char gender = cnp.charAt(CNP_POZITIE_GEN);
        for (int i = 0; i < CNP_BARBATI.length; i++) {
            if (CNP_BARBATI[i] == gender) {
                return true;
            }
        }
        return false;
    }


    public Person(String pCNP, String pNume, String pPrenume, double pSalariul) {

        cnp = pCNP;
        nume = pNume;
        prenume = pPrenume;
        salariul = pSalariul;

        String sYear = "" + cnp.charAt(1) + cnp.charAt(2);
        int year = Integer.valueOf(sYear);
        if (cnp.charAt(0) == '1' || cnp.charAt(0) == '2') {
            year = year + 1900;
        } else {
            year = year + 2000;
        }

        int month;
        String sMonth = "" + cnp.charAt(3) + cnp.charAt(4);
        month = Integer.valueOf(sMonth);

        int day;
        String sDay = "" + cnp.charAt(5) + cnp.charAt(6);
        day = Integer.valueOf(sDay);

        dataNasterii = LocalDate.of(year, month, day);





        }










    public LocalDate getDataNasterii(){
    return dataNasterii;
    }


    public String getCnp() {
        return cnp;
    }

    public void setCnp(String noulCnp) {
        cnp = noulCnp;
    }


    public String getNume() {
        return nume;
    }

    public void setNume(String noulNume) {
        nume = noulNume;
    }


    public double getSalariul() {
        return salariul;
    }


    public String getPrenume() {
        return prenume;
    }

    public void setPrenume(String noulPrenume) {
        prenume = noulPrenume;
    }


    public static double getSalariulMinim() {
        return salariulMinim;
    }


    public void setSalariul(double noulSalariul) {
        if (noulSalariul < salariulMinim) {
            System.out.println(" ");
        } else {
            salariul = noulSalariul;
        }
    }


    public static void setSalariuMinim(double noulSalariuMinim) {
        if (noulSalariuMinim < salariulMinim) {
            System.out.println(" Nu putem scadea salariul minim pe economie de la" +
                    salariulMinim + " la " + noulSalariuMinim);
        } else {
            salariulMinim = noulSalariuMinim;
        }
    }



}
