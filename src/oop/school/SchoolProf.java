package oop.school;

import java.sql.SQLOutput;

public class SchoolProf {

    // Creați o structura de date care sa poate stoca informatii despre un profesor

    // nume, materia pe care o predă, salariul de baza, daca este diriginte, vechime în ani

    // Să pot crea un profesor nou cu cel putin nume + materie (vizibila doar in pachetul school)
    // Să pot crea un profesor nou cu toate informatiile (vizibila oriunde)

     public String nume;
    public String materie;
    public double salariu;
    public boolean diriginte;
    public int vechime;
    public String grad;

    SchoolProf () {

    }
    public SchoolProf ( String pNume, String pMaterie, double pSalariu, boolean pDiriginte, int pVechime) {
        nume = pNume;
        materie=pMaterie;
        salariu=pSalariu;
        diriginte = pDiriginte;
        vechime= pVechime;

    }

    //Creati o metoda in clasa Teacher cu ajutprul careia profesorul sa se prezinte.
    //  ex "Bună ziua! Numele meu este Pop Ion și preadau Matematica de 5 ani. Eu sunt diriginte"
    //Apelati medota respectiva din clasa Main


    public String prezintate(){
        if (diriginte == true ) {
            return " Salut! Eu sunt " + nume +
                    " si predau " + materie +
                    " de " + vechime + " ani ." +
                    " castig " + salariu + " eu sunt diriginte";
        }
        else {
        return " Salut! Eu sunt " + nume +
                " si predau " + materie +
                " de " + vechime + " ani ." +
                " castig " + salariu + " eu Nu sunt diriginte";

    }






}}








